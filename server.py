from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///records.db'
db = SQLAlchemy(app)

class ColorFormat():
    def get_color(self, wn8):
        if wn8 <= 300:
            return '#820d0d'
        elif wn8 <= 449:
            return '#cd3333'
        elif wn8 <= 649:
            return '#cc7a00'
        elif wn8 <= 899:
            return '#ccb800'
        elif wn8 <= 1199:
            return '#849b24'
        elif wn8 <= 1599:
            return '#4d7326'
        elif wn8 <= 1999:
            return '#4099bf'
        elif wn8 <= 2499:
            return '#3972c6'
        elif wn8 <= 2899:
            return '#793db6'
        else:
            return '#401070'

class TankValues():
    json_data = {}
    def __init__(self):
        self.load_data()
    
    def refresh(self):
        self.load_data()
        
    def load_data(self):
        import json
        
        file_json = open('expected_tank_values.json', 'r') 
        try: 
            self.json_data = json.load(file_json) 
        except Exception, e: 
            print(e.strerror)
            pass
        file_json.close() 
    
    def get_values(self, comp):
        for tank in self.json_data['data']:
            if int(tank['IDNum']) == comp:
                return tank
        return None
        
class TankInfo():
    json_data = {}
    def __init__(self):
        self.load_data()
    
    def refresh(self):
        self.load_data()
        
    def load_data(self):
        import json
        
        file_json = open('tanks.json', 'r') 
        try: 
            self.json_data = json.load(file_json) 
        except Exception, e: 
            print(e.strerror)
            pass
        file_json.close() 
    
    def fetch_tank_info(self, cid, tid):
        for tank in self.json_data:
            if tank['tankid'] is tid and tank['countryid'] is cid:
                return tank
        return None

#db.create_all()
tankValues = TankValues()
tankInfo = TankInfo()
colors = ColorFormat()

class Battle(db.Model):
    __tablename__ = 'Battle'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    uid = db.Column(db.Integer, unique=True)
    map = db.Column(db.Text)
    started = db.Column(db.Integer)
    victory = db.Column(db.Boolean)
    
    def __init__(self, id, uid, map, started, victory):
        self.id = id
        self.uid = uid
        self.map = map
        self.started = started
        self.victory = victory

    def __str__(self):
        return "{0} - {1} on {2}".format(str(self.id), "Victory" if self.victory else "Defeat", self.map)

class BattlePlayer(db.Model):
    __tablename__ = 'BattlePlayer'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    bid = db.Column(db.Integer)
    countryid = db.Column(db.Integer)
    tankid = db.Column(db.Integer)
    accountid = db.Column(db.Integer)
    detected = db.Column(db.Integer)
    trackingDamage = db.Column(db.Integer)
    damageDealt = db.Column(db.Integer)
    spottingDamage = db.Column(db.Integer)
    exp = db.Column(db.Integer)
    capPoints = db.Column(db.Integer)
    defensePoints = db.Column(db.Integer)
    frags = db.Column(db.Integer)
    credits = db.Column(db.Integer)
    team = db.Column(db.Integer)
    
    def __init__(self, id, bid, countryid, tankid, accountid, detected, trackingDamage, damageDealt, spottingDamage, exp, capPoints, defensePoints, frags, credits, team):
        self.id = id
        self.bid = bid
        self.countryid = countryid
        self.tankid = tankid
        self.accountid = accountid
        self.detected = detected
        self.trackingDamage = trackingDamage
        self.damageDealt = damageDealt
        self.spottingDamage = spottingDamage
        self.exp = exp
        self.capPoints = capPoints
        self.defensePoints = defensePoints
        self.frags = frags
        self.credits = credits
        self.team = team
        
    def __str__(self):
        tank = tankInfo.fetch_tank_info(self.countryid, self.tankid)
        tankName = "Unknown vehicle"
        if tank:
            tankName = tank['title']
        return "{0} - {1} exp, {2} credits".format(tankName, str(self.exp), str(self.credits))
        
    def get_wn8(self):
        tank = tankInfo.fetch_tank_info(self.countryid, self.tankid)
        expected_values = tankValues.get_values(tank['compDescr'])
        wn8_val = 0
        if expected_values:
            expected_damage = self.damageDealt / expected_values['expDamage']
            expected_spotting = self.detected / expected_values['expSpot']
            expected_frags = self.frags / expected_values['expFrag']
            expected_defense = self.defensePoints / expected_values['expDef']
            
            c_win = max(0, (1 - .71) / (1 - .71))
            c_damage = max(0, (expected_damage - .22) / (1 - .22))
            c_frags = max(0, min(c_damage + .2, (expected_frags - .12) / (1 - .12)))
            c_spots = max(0, min(c_damage + .1, (expected_spotting - .1) / (1 - .1)))
            c_defense = max(0, min(c_damage + .1, (expected_defense - .1) / (1 - .1)))
            
            wn8_val = (980 * c_damage) + (210 * c_damage * c_frags) + (155 * c_frags * c_spots) + (75 * c_defense * c_frags) + (145 * min(1.8, c_win))
            wn8_val = int(round(wn8_val))
        return wn8_val

#class PlayerInfo(db.Model):
#    id = db.Column(db.Integer, db.ForeignKey('BattlePlayer.accountid'), primary_key=True)
#    name = db.Column(db.Text)
#    
#    def __init__(self, id, name):
#        self.id = id
#        self.name = name

@app.route('/')
def index():
    battlesWon = Battle.query.filter_by(victory = True).count()
    battlesTotal = Battle.query.count()
    winRate = (battlesWon / (battlesTotal * 1.0)) * 100
    msg = "Win Rate: {0}</br>".format(str(winRate))
    battleResultMsg = ""
    totalWN8 = 0
    for battle in Battle.query.all():
        myResult = BattlePlayer.query.filter_by(accountid=1000262583, bid=battle.id).first()
        wn8 = myResult.get_wn8()
        totalWN8 += wn8
        battleResultMsg += "<span style='color:" + colors.get_color(wn8) + ";'>{0}: {1} - {2} WN8</span></br>".format(str(battle), str(myResult), str(wn8))
    averageWN8 = (totalWN8 / (battlesTotal * 1.0))
    msg += "Average WN8: {0}</br>".format(str(averageWN8))
    msg += "<span style='background-color:black;font-size: 16pt;'>" + battleResultMsg + "</span>"
    return msg

if __name__ == '__main__':
    app.debug = True
    app.run()