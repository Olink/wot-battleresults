import threading
import time
import os
import sqlite3
import signal

class DatabaseManager():
    connection = None
    def __init__(self):
        self.connect()
        try:
            cur = self.connection.cursor()
            cur.execute('CREATE TABLE IF NOT EXISTS Battle (id integer primary key autoincrement, uid integer unique, map text, started integer, victory bool)')
            cur.execute('CREATE TABLE IF NOT EXISTS BattlePlayer \
                (id integer primary key autoincrement, bid integer, countryid integer, tankid integer, accountid integer, detected integer, trackingDamage integer, \
                damageDealt integer, spottingDamage integer, exp integer, capPoints integer, defensePoints integer, \
                frags integer, credits integer, team integer)')
            cur.execute('CREATE TABLE IF NOT EXISTS PlayerInfo (id integer primary key, name text)')
            self.connection.commit()
        except sqlite3.Error as e:
            print(e.strerror)
        self.close()
    
    def connect(self):
        try:
            self.connection = sqlite3.connect('records.db') 
        except sqlite3.Error as e:
            print(e.strerror)
    
    def close(self):
        if self.connection:
            self.connection.close()

    def insert_battle(self, battle):
        self.connect()
        try:   
            cur = self.connection.cursor()
            cur.execute("INSERT INTO Battle(uid, map, started, victory) VALUES(?, ?, ?, ?)", (battle['arenaUniqueID'], battle['common']['arenaTypeName'], battle['common']['arenaCreateTime'], battle['common']['winnerTeam'] is battle['personal']['team']))
            cur.execute("SELECT last_insert_rowid()")
            battleId = cur.fetchone()[0]
            print(battleId)
            self.insert_players(cur, battle, battleId)
            self.connection.commit()
        except sqlite3.Error, e:
            print(e)
        self.close()

    def insert_players(self, cursor, battle, battleId):
        for k in battle['vehicles']:
            veh = battle['vehicles'][k]
            self.insert_player(cursor, veh, battleId)
            self.update_player(cursor, battle['players'][veh['accountDBID']], veh['accountDBID'])

    def insert_player(self, cursor, vehicle, battleId):
        cursor.execute("INSERT INTO BattlePlayer(bid, countryid, tankid, accountid, detected, trackingDamage, damageDealt, spottingDamage, exp, capPoints, defensePoints, frags, credits, team) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            (battleId, vehicle['countryID'], vehicle['tankID'], vehicle['accountDBID'], vehicle['spotted'], vehicle['damageAssistedTrack'], vehicle['damageDealt'], vehicle['damageAssistedRadio'], vehicle['xp'], vehicle['capturePoints'], vehicle['droppedCapturePoints'], vehicle['kills'], vehicle['credits'], vehicle['team']))

    def update_player(self, cursor, player, id):
        cursor.execute("INSERT OR IGNORE INTO PlayerInfo(id, name) VALUES (?, ?)", (id, player['name']))
        cursor.execute("UPDATE PlayerInfo SET id=?, name=? WHERE id=?", (id, player['name'], id))

class ExpectedJsonManager():
    json_data = {}
    def __init__(self):
        import json
        
        file_json = open('expected_tank_values.json', 'r') 
        try: 
            self.json_data = json.load(file_json) 
        except Exception, e: 
            print(e.strerror)
            pass
        file_json.close() 

    def lookup_data(self, veh_id):
        data = self.json_data['data']
        for veh in data:
            if veh['IDNum'] == veh_id:
                return veh
        return None

class FileWatcher(threading.Thread):
    dir_to_watch = os.getenv('APPDATA') + "/Wargaming.net/WorldOfTanks/battle_results/"
    old_files = []
    db = None
    jsonManager = None
    to_run = True
    
    def run(self):
        self.db = DatabaseManager()
        self.jsonManager = ExpectedJsonManager()
        while self.to_run:
            #print("Checking for file changes.")
            files = []
            
            for x in os.walk(self.dir_to_watch):
                if x[0] is self.dir_to_watch:
                    continue

                for y in os.walk(x[0]):
                    path = x[0] + "/"
                    file_paths = [path + filename for filename in [f for f in y[2] if f.endswith('.dat')]]
                    files.extend(file_paths)
                    
            dif = set(files) - set(self.old_files)
            for file in dif:
                print("One updated file: " + file)
                self.log_battle(file)

            #self.log_battle(files[0])
            self.old_files = files
            #to_run = False
            time.sleep(5)
    
    def shutdown(self, signum, frame):
        print("Halting...")
        self.to_run = False
        
    def log_battle(self, file):
        from ProcessBattleResults import process_file
        try:
            results = process_file(file)
            wn8s = self.calculate_wn8(results)
            import pprint
            pp = pprint.PrettyPrinter(indent=4)
            pp.pprint(wn8s)
            self.db.insert_battle(results)
        except RuntimeError as e:
            print(e.strerror)
    
    def calculate_wn8(self, results):
        wn8 = {}
        for k in results['vehicles']:
            veh = results['vehicles'][k]
            expected_values = self.jsonManager.lookup_data(veh['typeCompDescr'])
            wn8_val = 0
            if expected_values:
                expected_damage = veh['damageDealt'] / expected_values['expDamage']
                expected_spotting = veh['spotted'] / expected_values['expSpot']
                expected_frags = veh['kills'] / expected_values['expFrag']
                expected_defense = veh['droppedCapturePoints'] / expected_values['expDef']
                
                c_win = max(0, (1 - .71) / (1 - .71))
                c_damage = max(0, (expected_damage - .22) / (1 - .22))
                c_frags = max(0, min(c_damage + .2, (expected_frags - .12) / (1 - .12)))
                c_spots = max(0, min(c_damage + .1, (expected_spotting - .1) / (1 - .1)))
                c_defense = max(0, min(c_damage + .1, (expected_defense - .1) / (1 - .1)))
                
                wn8_val = (980 * c_damage) + (210 * c_damage * c_frags) + (155 * c_frags * c_spots) + (75 * c_defense * c_frags) + (145 * min(1.8, c_win))
                wn8_val = int(round(wn8_val))
            player_name = results['players'][veh['accountDBID']]['name']
            wn8[player_name] = wn8_val
        return wn8

thread = FileWatcher()
thread.daemon = True
thread.start()

try:
    while True:
        time.sleep(5000)
except KeyboardInterrupt:
    pass